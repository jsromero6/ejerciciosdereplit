# Read a list of integers:
a = [int(s) for s in input().split()]
# Print a value:
# print(a)
acum = 0
for i in range(1, len(a)-1):
  if a[i - 1] < a[i] > a[i + 1]:
    acum += 1
print(acum)