# Read a list of integers:
a = [int(s) for s in input().split()]
total = 1
for i in range(1, len(a)):
  if a[i-1] != a[i]:
    total += 1
print(total)
