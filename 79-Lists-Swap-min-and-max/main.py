# Read a list of integers:
a = [int(s) for s in input().split()]
max_i = a.index(max(a))
min_i = a.index(min(a))

a[min_i], a[max_i] = a[max_i], a[min_i]

print(a)