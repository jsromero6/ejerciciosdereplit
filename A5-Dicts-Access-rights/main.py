n = int(input())
com = {}
for i in range(n):
    myList = list(input().split())
    com[myList[0]] = myList[1:len(myList)]
m = int(input())
for t in range(m):
    opps = list(input().split())
    if 'W' in com[opps[1]] and opps[0] == 'write':
        print('OK')
    elif 'R' in com[opps[1]] and opps[0] == 'read':
        print('OK')
    elif 'X' in com[opps[1]] and opps[0] == 'execute':
        print('OK')
    else:
        print('Access denied')
