n = int(input())
a = [["."]*n for i in range(n)]
for y in range(n):
    for x in range(n):
        if x == y:
            a[y][x] = '*'
        elif x == (n-1)/2:
            a[y][x] = '*'
        elif x + y == 6:
            a[y][x] = '*'
        elif y == (n-1)/2:
            a[y][x] = '*'
for line in a:
    print(*line)