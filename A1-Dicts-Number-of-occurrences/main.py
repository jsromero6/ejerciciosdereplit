s = input().split()
times_seen = {}
for i in s:
  if i not in times_seen:
    times_seen[i] = 0
  print(times_seen[i], end=' ')
  times_seen[i] += 1