n, m = [int(i) for i in input().split()]
a = [[int(j) for j in input().split()] for i in range(n)]
times = int(input())

for x in range(n):
  for y in range(m):
    a[x][y] = ((a[x][y]) * times)

for row in a:
  print(' '.join([str(a) for a in row]))
